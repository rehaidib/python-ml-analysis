#!/bin/sh
# Runs the analyser's Docker container, mounting the required directories.
# Also creates these directories on in the current folder, if necessary.
# Any arguments passed to this script will be passed on to `docker run` after specifying the image to run.
# i.e., to run bash inside the container, use `./run.sh bash`

ensure_dir() {
  mkdir -p "$1"
}

# cd to the directory in which this script is stored
cd $(dirname $0)

pip_cache_dir="$(pwd)/.cache/pip"
projects_dir="$(pwd)/projects"
results_dir="$(pwd)/results"
data_dir="$(pwd)/data"

ensure_dir $pip_cache_dir
ensure_dir $projects_dir
ensure_dir $results_dir

docker run -it --rm \
  -v $pip_cache_dir:/home/python-ml-analyser/.cache/pip \
  -v $projects_dir:/app/projects \
  -v $results_dir:/app/results \
  -v $data_dir:/app/data \
  python-ml-analyser $@
