#=-- Builder image
FROM golang:1.15-buster as builder
WORKDIR /app

# Download dependencies
COPY analyser/go.mod analyser/go.sum ./
RUN go mod download

# Build analyser
COPY analyser ReadMe.md ./
RUN go build -o analyser ./main

#=-- Final image
FROM python:3.7-buster
WORKDIR /app
ARG user_id
#ARG group_id

# Add user and group with specified user and group IDs and with a home directory for the user.
#RUN groupadd -g $group_id python-ml-analyser && useradd -l -m -g $user_id python-ml-analyser
#RUN useradd -l -m -g $user_id python-ml-analyser
RUN useradd python-ml-analyser
# Install some C build dependencies
RUN apt-get update \
    && apt-get -y install libmpfr-dev libmpc-dev libopenmpi-dev \
    && rm -rf /var/lib/apt/lists/*

# Install the Python dependencies of the analyser.
COPY requirements.txt ./
RUN pip install -r requirements.txt

USER python-ml-analyser:python-ml-analyser

# Disable SSH host key verification when cloning through SSH, so we can clone without having
# to pass 'yes' when asked if we trust the host key. TODO: if I ever still use this: check the permissions for the file.
#RUN mkdir -p ~/.ssh && echo '\
#Host * \n\
#    StrictHostKeyChecking no \n\
#    UserKnownHostsFile /dev/null'\
#> ~/.ssh/config

COPY --from=builder /app/analyser ./

# Not entirely sure which package required this, but it solves an otherwise broken `pip install` of that package.
ENV TORCH_CUDA_ARCH_LIST="6.0;6.1;6.2;7.0;7.5"

CMD ./analyser
