#!/bin/bash
# Prints some statistics based on how I commented my dataset.yml :)
# Default dataset file to get stats of is dataset.yml, but can be chosen with an argument too
# Example: ./stats.sh other-dataset.yml

dsfile="$(dirname $0)/dataset.yml"
[[ "$1" != "" ]] && dsfile="$1"

[[ -d "$dsfile" ]] && echo "dataset file is a directory: $dsfile" && exit 1
[[ ! -f "$dsfile" ]] && echo "dataset file does not exist: $dsfile" && exit -1

function calc_n_projects {
  n_urls=$(grep -e "^- url: .*" $dsfile | wc -l)
  n_folders=$(grep -e "^\s*- folder: .*" $dsfile | wc -l)

  n_projects_defs=$(grep -e "^\s*projects:.*" $dsfile | wc -l)
  n_projects_with_folders=$(($n_projects_defs - 1))

  echo $(($n_urls + $n_folders - $n_projects_with_folders))
}

function calc_n_recursion_err {
  grep -e "^\s*skip: true # RecursionError" $dsfile | wc -l
}

function calc_n_add_reqs {
  grep -e "^\s*skip: true # AdditionalRequirements needed" $dsfile | wc -l
}

function calc_n_skipped_other {
  grep -e "^\s*skip: true #.*" $dsfile | grep -v "RecursionError" | grep -v "AdditionalRequirements" | wc -l
}

function calc_n_skipped_none {
  grep -e "^\s*skip: true.*" $dsfile | grep -v "#" | wc -l
}

n_projects=$(calc_n_projects)
n_recursion_err=$(calc_n_recursion_err)
n_add_reqs=$(calc_n_add_reqs)
n_skipped_other=$(calc_n_skipped_other)
n_skipped_none=$(calc_n_skipped_none)
n_working=$(($n_projects - $n_recursion_err - $n_add_reqs - $n_skipped_other - $n_skipped_none))

echo "---"
echo "Dataset stats for file: $dsfile"
echo "+ Total number of projects in dataset:                   $n_projects"
echo "- Number of projects with RecursionError:                $n_recursion_err"
echo "- Number of projects with AdditionalRequirements needed: $n_add_reqs"
echo "- Number of projects skipped for other reasons:          $n_skipped_other"
echo "- Number of projects skipped with no reason:             $n_skipped_none"
echo "---"
echo "= Number of working projects:                            $n_working"
echo "---"
