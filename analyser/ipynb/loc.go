package ipynb

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
)

var ErrPartialCount = errors.New("one or more Jupyter Notebooks could not be counted")

type JupyterNotebook struct {
	Cells []JupyterNotebookCell `json:"cells"`
}

func (ipynb *JupyterNotebook) UnmarshalJSON(data []byte) error {
	var nbjson struct {
		Cells []json.RawMessage
	}
	if err := json.Unmarshal(data, &nbjson); err != nil {
		return err
	}

	ipynb.Cells = []JupyterNotebookCell{}
	for _, jsonCell := range nbjson.Cells {
		var csa cellSourceArray
		if err := json.Unmarshal(jsonCell, &csa); err == nil {
			ipynb.Cells = append(ipynb.Cells, csa)
			continue
		}

		var css cellSourceString
		if err := json.Unmarshal(jsonCell, &css); err == nil {
			ipynb.Cells = append(ipynb.Cells, css)
			continue
		}

		// we could signal some kind of error here, but let's just ignore it
	}

	return nil
}

type JupyterNotebookCell interface {
	GetType() string
	GetSource() []string
}

type cellSourceArray struct {
	Type   string   `json:"cell_type"`
	Source []string `json:"source"`
}

func (csa cellSourceArray) GetType() string {
	return csa.Type
}

func (csa cellSourceArray) GetSource() []string {
	return csa.Source
}

type cellSourceString struct {
	Type   string `json:"cell_type"`
	Source string `json:"source"`
}

func (css cellSourceString) GetType() string {
	return css.Type
}

func (css cellSourceString) GetSource() []string {
	return strings.Split(css.Source, "\n")
}

func Open(filename string) (*JupyterNotebook, error) {
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var nb JupyterNotebook
	if err := json.Unmarshal(contents, &nb); err != nil {
		return nil, fmt.Errorf("failed to parse JSON: %w", err)
	}
	return &nb, nil
}

func (nb *JupyterNotebook) CountLoC() int {
	total := 0
	for _, cell := range nb.Cells {
		if cell.GetType() == "code" {
			total += len(cell.GetSource())
		}
	}
	return total
}

func CountLoC(filename string) (int, error) {
	nb, err := Open(filename)
	if err != nil {
		return 0, err
	}

	return nb.CountLoC(), nil
}

func CountTotalLoC(filenames []string) (int, error) {
	total := 0
	filesWithErrors := []string{}
	for _, filename := range filenames {
		count, err := CountLoC(filename)
		if err != nil {
			filesWithErrors = append(filesWithErrors, filename)
		}
		total += count
	}

	if len(filesWithErrors) > 0 {
		return total, fmt.Errorf("%w: %+v", ErrPartialCount, filesWithErrors)
	}
	return total, nil
}
