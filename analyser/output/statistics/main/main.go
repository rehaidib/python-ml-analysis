package main

import (
	"fmt"
	"os"

	"github.com/fatih/color"
	"gitlab.com/bvobart/python-ml-analysis/output/statistics"
)

func main() {
	filename := "./output/csv/test-resources/example-results.yml"
	if len(os.Args) > 1 {
		filename = os.Args[1]
	}

	fmt.Println("Getting statistics on dataset file:", color.GreenString(filename))

	stats, err := statistics.FromResultsFilename(filename)
	if err != nil {
		panic(err)
	}

	fmt.Println()
	stats.Print()
	fmt.Println()

	port := 5000
	fmt.Println("Hosting dataset statistics & plots on port", color.GreenString("%d", port))
	stats.HostCharts(port)
}
