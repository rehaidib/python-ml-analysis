package statistics

import (
	"fmt"
	"io"
	"net/http"

	"github.com/fatih/color"
	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
	"github.com/go-echarts/go-echarts/v2/types"
)

// ------------------------------------------------------------------

func (stats *ResultsFileStats) HostCharts(port int) {
	rfc := newResultsFileCharts(stats)
	http.ListenAndServe(fmt.Sprint(":", port), &rfc)
}

type resultsFileCharts struct {
	stats *ResultsFileStats
}

func newResultsFileCharts(stats *ResultsFileStats) resultsFileCharts {
	return resultsFileCharts{stats}
}

// ------------------------------------------------------------------

func (rfc *resultsFileCharts) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	if err := rfc.RenderAll(w); err != nil {
		fmt.Println("Error:", err)
	}
}

func (rfc *resultsFileCharts) RenderAll(w io.Writer) error {
	err := rfc.renderGitCommits(w)
	if err != nil {
		return err
	}

	err = rfc.renderGitContributors(w)
	if err != nil {
		return err
	}

	err = rfc.renderNumFiles(w)
	if err != nil {
		return err
	}

	err = rfc.renderNumLines(w)
	if err != nil {
		return err
	}
	// TODO: more stats

	return nil
}

// ------------------------------------------------------------------

var showAllAxisLabels = charts.WithXAxisOpts(opts.XAxis{
	AxisLabel: &opts.AxisLabel{Interval: "0"},
})

var showAllSeriesLabels = charts.WithLabelOpts(opts.Label{
	Show:     true,
	Position: "right",
})

var showLegend = charts.WithLegendOpts(opts.Legend{
	Show: true,
	Left: "25%",
})

var showMinMaxAvgLines = charts.WithMarkLineNameTypeItemOpts(
	opts.MarkLineNameTypeItem{Name: "Minimum", Type: "min"},
	opts.MarkLineNameTypeItem{Name: "Maximum", Type: "max"},
	opts.MarkLineNameTypeItem{Name: "Average", Type: "average"},
)

var showZoomSlider = charts.WithDataZoomOpts(opts.DataZoom{
	Type: "slider",
})

var showToolbox = charts.WithToolboxOpts(opts.Toolbox{
	Show:  true,
	Right: "25%",
	Feature: &opts.ToolBoxFeature{
		SaveAsImage: &opts.ToolBoxFeatureSaveAsImage{
			Show:  true,
			Type:  "png",
			Title: "Save to PNG",
		},
		DataView: &opts.ToolBoxFeatureDataView{
			Show:  true,
			Title: "View raw data",
			Lang:  []string{"Raw data", "Close", "Refresh"},
		},
		DataZoom: &opts.ToolBoxFeatureDataZoom{
			Show: true,
			Title: map[string]string{
				"zoom": "Select a rectangular area to zoom in on.",
				"back": "Restore rectangle zoom area",
			},
		},
	},
})

// ------------------------------------------------------------------

func makeInitOptions(numProjects int, doubleHeight bool) charts.GlobalOpts {
	if numProjects == 0 {
		color.Red("ERROR: number of projects is 0, charts will have zero width")
	}

	// each project gets a line 15 pixels in height, or double that if the chart has two bars per project.
	height := 15 * numProjects
	if doubleHeight {
		height *= 2
	}

	return charts.WithInitializationOpts(opts.Initialization{
		Width:  "100%",
		Height: fmt.Sprint(height, "px"),
		Theme:  types.ThemeChalk,
	})
}

func makeTitleOptions(title string, subtitle string) charts.GlobalOpts {
	return charts.WithTitleOpts(opts.Title{
		Title:    title,
		Subtitle: subtitle,
		Left:     "center",
	})
}

func makeYAxisOptions(label string) charts.GlobalOpts {
	return charts.WithYAxisOpts(opts.YAxis{
		Name: label,
		AxisLabel: &opts.AxisLabel{
			Interval: "0", // show all project names
		},
	})
}

func makeXAxisOptions(label string, formatter string) charts.GlobalOpts {
	return charts.WithXAxisOpts(opts.XAxis{
		Name:      label,
		SplitLine: &opts.SplitLine{Show: true},
		AxisLabel: &opts.AxisLabel{
			Formatter: formatter,
		},
	})
}

// ------------------------------------------------------------------

func (rfc resultsFileCharts) renderGitContributors(w io.Writer) error {
	bar := charts.NewBar()

	bar.SetGlobalOptions(
		showLegend,
		showToolbox,
		makeInitOptions(rfc.stats.NumProjects, false),
		makeTitleOptions("Git Contributors", "Number of contributors per project, i.e. distinct commit authors."),
		makeYAxisOptions("Projects"),
		makeXAxisOptions("Contributors", "{value} contributors"),
	)

	bar.SetXAxis(rfc.stats.ProjectNames)

	bar.AddSeries("Contributors", rfc.stats.generateGitContributorData())
	bar.SetSeriesOptions(
		showAllSeriesLabels,
		showMinMaxAvgLines,
	)

	bar.XYReversal()
	return bar.Render(w)
}

func (rfc resultsFileCharts) renderGitCommits(w io.Writer) error {
	bar := charts.NewBar()

	bar.SetGlobalOptions(
		showLegend,
		showToolbox,
		makeInitOptions(rfc.stats.NumProjects, false),
		makeTitleOptions("Git Commits", "Number of commits per project"),
		makeYAxisOptions("Projects"),
		makeXAxisOptions("Commits", "{value} commits"),
	)

	bar.SetXAxis(rfc.stats.ProjectNames)

	bar.AddSeries("Commits", rfc.stats.generateGitCommitData())
	bar.SetSeriesOptions(
		showAllSeriesLabels,
		showMinMaxAvgLines,
	)

	bar.XYReversal()
	return bar.Render(w)
}

func (rfc resultsFileCharts) renderNumFiles(w io.Writer) error {
	bar := charts.NewBar()

	bar.SetGlobalOptions(
		showLegend,
		showToolbox,
		makeInitOptions(rfc.stats.NumProjects, true),
		makeTitleOptions("Number of Files", "Number of pure Python and Jupyter Notebook files per project"),
		makeYAxisOptions("Projects"),
		makeXAxisOptions("Files", "{value} files"),
	)

	bar.SetXAxis(rfc.stats.ProjectNames)

	bar.AddSeries("Pure Python", rfc.stats.generateNumPythonFilesData())
	bar.AddSeries("Jupyter Notebooks", rfc.stats.generateNumIPynbFilesData())
	bar.SetSeriesOptions(
		showAllSeriesLabels,
		showMinMaxAvgLines,
	)

	bar.XYReversal()
	return bar.Render(w)
}

func (rfc resultsFileCharts) renderNumLines(w io.Writer) error {
	bar := charts.NewBar()

	bar.SetGlobalOptions(
		showLegend,
		showToolbox,
		makeInitOptions(rfc.stats.NumProjects, true),
		makeTitleOptions("Number of Lines", "Number of lines of pure Python code and Python code embedded in Jupyter Notebooks per project"),
		makeYAxisOptions("Projects"),
		makeXAxisOptions("Lines of code", "{value} lines"),
	)

	bar.SetXAxis(rfc.stats.ProjectNames)

	bar.AddSeries("Pure Python", rfc.stats.generateNumPythonLinesData())
	bar.AddSeries("Jupyter Notebooks", rfc.stats.generateNumIPynbLinesData())
	bar.SetSeriesOptions(
		showAllSeriesLabels,
		showMinMaxAvgLines,
	)

	bar.XYReversal()
	return bar.Render(w)
}

func (stats ResultsFileStats) generateGitContributorData() []opts.BarData {
	res := []opts.BarData{}
	for _, n := range stats.Contributors.All {
		res = append(res, opts.BarData{Value: n})
	}
	return res
}

func (stats ResultsFileStats) generateGitCommitData() []opts.BarData {
	res := []opts.BarData{}
	for _, n := range stats.Commits.All {
		res = append(res, opts.BarData{Value: n})
	}
	return res
}

func (stats ResultsFileStats) generateNumPythonFilesData() []opts.BarData {
	res := []opts.BarData{}
	for _, n := range stats.PythonFiles.All {
		res = append(res, opts.BarData{Value: n})
	}
	return res
}

func (stats ResultsFileStats) generateNumIPynbFilesData() []opts.BarData {
	res := []opts.BarData{}
	for _, n := range stats.IPynbFiles.All {
		res = append(res, opts.BarData{Value: n})
	}
	return res
}

func (stats ResultsFileStats) generateNumPythonLinesData() []opts.BarData {
	res := []opts.BarData{}
	for _, n := range stats.PythonLines.All {
		res = append(res, opts.BarData{Value: n})
	}
	return res
}

func (stats ResultsFileStats) generateNumIPynbLinesData() []opts.BarData {
	res := []opts.BarData{}
	for _, n := range stats.IPynbLines.All {
		res = append(res, opts.BarData{Value: n})
	}
	return res
}
