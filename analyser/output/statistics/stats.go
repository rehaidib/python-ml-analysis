package statistics

import (
	"errors"
	"fmt"

	"github.com/fatih/color"
	"github.com/montanaflynn/stats"

	"gitlab.com/bvobart/python-ml-analysis/output"
	"gitlab.com/bvobart/python-ml-analysis/pylint"
)

var ErrEmptyResults = errors.New("empty results file")

type stat struct {
	All []float64
	// Minimum value
	Min float64
	// Mean value
	Mean float64
	// Median value
	Median float64
	// Max value
	Max float64
	// 25th Percentile
	Perc25 float64
	// 75th Percentile
	Perc75 float64
	// Standard deviation
	StdDev float64
	// Sum of all values
	Sum float64
}

// ResultsFileStats describes exploratory statistics on the output of the analysis of all projects in the dataset.
type ResultsFileStats struct {
	NumProjects              int
	ProjectNames             []string
	Contributors             stat
	Commits                  stat
	PythonFiles              stat
	IPynbFiles               stat
	PythonLines              stat
	IPynbLines               stat
	PythonLinesPerFile       stat
	IPynbLinesPerFile        stat
	NumMessages              map[pylint.MessageType]stat
	Top20Symbols             map[pylint.MessageType]PairList
	HadRequirementsFile      int
	HadExtraRequirements     int
	HadOnlyExtraRequirements int
	HadAlsoExtraRequirements int
}

func FromResultsFilename(filename string) (*ResultsFileStats, error) {
	resfile, err := output.FromYAMLFile(filename)
	if err != nil {
		return nil, err
	}

	return FromResultsFile(*resfile)
}

func FromResultsFile(resfile output.ResultsFile) (*ResultsFileStats, error) {
	if len(resfile.Results) == 0 {
		return nil, ErrEmptyResults
	}

	names := []string{}
	commits := []float64{}
	contributors := []float64{}
	pyfiles := []float64{}
	ipynbfiles := []float64{}
	pylines := []float64{}
	ipynblines := []float64{}
	pylinesPerFile := []float64{}
	ipynblinesPerFile := []float64{}

	numMessages := make(map[pylint.MessageType][]float64, 0)
	symbolsCount := make(map[pylint.MessageType]map[string]int, 0)
	hadRequirementsFile := 0
	hadExtraRequirements := 0
	hadOnlyExtraRequirements := 0 // so correctly specified requirements file, but needed extraRequirements
	hadAlsoExtraRequirements := 0 // so custom or generated requirements.txt and also needed extraRequirements
	for _, result := range resfile.Results {
		names = append(names, result.ProjectName)
		commits = append(commits, float64(result.NumCommits))
		contributors = append(contributors, float64(result.NumContributors))
		pyfiles = append(pyfiles, float64(result.NumPythonFiles))
		ipynbfiles = append(ipynbfiles, float64(result.NumIPynbFiles))
		pylines = append(pylines, float64(result.NumPythonLines))
		ipynblines = append(ipynblines, float64(result.NumIPynbLines))
		pylinesPerFile = append(pylinesPerFile, float64(result.NumPythonLines)/float64(result.NumPythonFiles))
		if result.NumIPynbFiles != 0 {
			ipynblinesPerFile = append(ipynblinesPerFile, float64(result.NumIPynbLines)/float64(result.NumIPynbFiles))
		}

		if result.HasRequirementsFile {
			hadRequirementsFile++
		}

		if result.HasRequirementsFile && result.HasExtraRequirements {
			hadOnlyExtraRequirements++
		}

		if !result.HasRequirementsFile && result.HasExtraRequirements {
			hadAlsoExtraRequirements++
		}

		if result.HasExtraRequirements {
			hadExtraRequirements++
		}

		for _, c := range pylint.MessageTypes {
			category := pylint.MessageType(c)
			msgsByCategory := pylint.MessageList(result.LintMessages).ByType(category)
			numMessages[category] = append(numMessages[category], float64(len(msgsByCategory)))
			symbolsCount[category] = AddCounts(symbolsCount[category], countSymbols(msgsByCategory))
		}
	}

	numMessagesStats := make(map[pylint.MessageType]stat, 0)
	for category, numMsgs := range numMessages {
		numMessagesStats[category] = makeStat(numMsgs)
	}

	return &ResultsFileStats{
		NumProjects:              len(resfile.Results),
		ProjectNames:             names,
		Commits:                  makeStat(commits),
		Contributors:             makeStat(contributors),
		PythonFiles:              makeStat(pyfiles),
		IPynbFiles:               makeStat(ipynbfiles),
		PythonLines:              makeStat(pylines),
		IPynbLines:               makeStat(ipynblines),
		PythonLinesPerFile:       makeStat(pylinesPerFile),
		IPynbLinesPerFile:        makeStat(ipynblinesPerFile),
		NumMessages:              numMessagesStats,
		Top20Symbols:             getTop20PerCategory(symbolsCount),
		HadRequirementsFile:      hadRequirementsFile,
		HadExtraRequirements:     hadExtraRequirements,
		HadOnlyExtraRequirements: hadOnlyExtraRequirements,
		HadAlsoExtraRequirements: hadAlsoExtraRequirements,
	}, nil
}

func countSymbols(msgsByCategory pylint.MessageList) map[string]int {
	counts := make(map[string]int, 0)
	for _, msg := range msgsByCategory {
		counts[msg.Symbol]++
	}
	return counts
}

func getTop20PerCategory(symbolsCount map[pylint.MessageType]map[string]int) map[pylint.MessageType]PairList {
	top20s := make(map[pylint.MessageType]PairList, len(pylint.MessageTypes))
	for _, c := range pylint.MessageTypes {
		category := pylint.MessageType(c)
		top20s[category] = Top20(symbolsCount[category])
	}
	return top20s
}

func (stats ResultsFileStats) Print() {
	fmt.Println("Total number of projects:", color.GreenString("%d", stats.NumProjects))
	fmt.Println()
	fmt.Println("Commits:")
	stats.Commits.Print()
	fmt.Println("Contributors:")
	stats.Contributors.Print()
	fmt.Println("Python files:")
	stats.PythonFiles.Print()
	fmt.Println("Jupyter Notebook files:")
	stats.IPynbFiles.Print()
	fmt.Println("Python lines of code:")
	stats.PythonLines.Print()
	fmt.Println("Jupyter Notebook lines of embedded Python code:")
	stats.IPynbLines.Print()
	fmt.Println("Avg. lines of Python code per Python file:")
	stats.PythonLinesPerFile.Print()
	fmt.Println("Avg. lines of Jupyter Notebook Python per Jupyter Notebook file:")
	stats.IPynbLinesPerFile.Print()

	fmt.Println("--- Dependency Management ---")
	fmt.Println("Number of projects ...")
	fmt.Println("  ... with working requirements.txt:", color.GreenString("%d", stats.HadRequirementsFile), "/", color.GreenString("%d", stats.NumProjects))
	fmt.Println("  ... with extraRequirements:", color.GreenString("%d", stats.HadExtraRequirements), "/", color.GreenString("%d", stats.NumProjects))
	fmt.Println("  ... with working requirements.txt and extraRequirements:", color.GreenString("%d", stats.HadOnlyExtraRequirements), "/", color.GreenString("%d", stats.NumProjects))
	fmt.Println("  ... with custom requirements.txt and extraRequirements:", color.GreenString("%d", stats.HadAlsoExtraRequirements), "/", color.GreenString("%d", stats.NumProjects))
	fmt.Println()

	fmt.Println("--- Pylint Messages ---")
	for category, stat := range stats.NumMessages {
		fmt.Println("Category:", color.YellowString(string(category)))
		stat.Print()
	}

	fmt.Println("- Top 20 Symbols per category -")
	for category, top20 := range stats.Top20Symbols {
		fmt.Println("Category:", color.YellowString(string(category)))
		fmt.Println("- ", color.GreenString("%s", top20))
	}
}

func (stat stat) Print() {
	fmt.Println("- min:", color.GreenString("%.0f", stat.Min))
	fmt.Println("- Q1:", color.GreenString("%.1f", stat.Perc25))
	fmt.Println("- Q2:", color.GreenString("%.1f", stat.Median))
	fmt.Println("- Q3:", color.GreenString("%.1f", stat.Perc75))
	fmt.Println("- max:", color.GreenString("%.0f", stat.Max))
	fmt.Println("- mean:", color.GreenString("%.1f", stat.Mean))
	fmt.Println("- Std. dev.:", color.GreenString("%.2f", stat.StdDev))
	fmt.Println("- Sum: ", color.GreenString("%.0f", stat.Sum))
	fmt.Println()
}

// makeStat creates a stat object from the given list of values by calculating the min, max, mean and medians.
func makeStat(values []float64) stat {
	min, _ := stats.Min(values)
	max, _ := stats.Max(values)
	mean, _ := stats.Mean(values)
	median, _ := stats.Median(values)
	perc25, _ := stats.Percentile(values, 25)
	perc75, _ := stats.Percentile(values, 75)
	stddev, _ := stats.StandardDeviation(values)
	sum, _ := stats.Sum(values)
	return stat{
		All:    values,
		Min:    min,
		Max:    max,
		Mean:   mean,
		Median: median,
		Perc25: perc25,
		Perc75: perc75,
		StdDev: stddev,
		Sum:    sum,
	}
}
