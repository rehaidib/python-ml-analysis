package statistics_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/bvobart/python-ml-analysis/output/statistics"
)

const testResFilename = "../csv/test-resources/example-results.yml"

func TestStatsPrint(t *testing.T) {
	statts, err := statistics.FromResultsFilename(testResFilename)
	require.NoError(t, err)
	require.NotNil(t, statts)
	statts.Print()
}
