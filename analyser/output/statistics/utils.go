package statistics

import (
	"fmt"
	"sort"
)

func AddCounts(counts map[string]int, add map[string]int) map[string]int {
	if counts == nil {
		return add
	}

	res := make(map[string]int, len(counts))
	for key, count := range counts {
		res[key] += count
	}
	for key, count := range add {
		res[key] += count
	}
	return res
}

func Top20(counts map[string]int) PairList {
	sorted := SortMostFrequent(counts)
	if len(sorted) <= 20 {
		return sorted
	}
	return sorted[:20]
}

func SortMostFrequent(counts map[string]int) PairList {
	pairs := PairList{}
	for item, count := range counts {
		pairs = append(pairs, Pair{item, count})
	}

	sort.Sort(mostFrequent(pairs))
	return pairs
}

type Pair struct {
	Item  string
	Count int
}

type PairList []Pair

func (ps PairList) String() string {
	res := "["
	for _, pair := range ps {
		res += fmt.Sprint(pair.Item, ":", pair.Count, " ")
	}
	return res + "]"
}

// Sorting strategies.
// mostFrequent sorts by item with highest count.
type mostFrequent []Pair

func (count mostFrequent) Len() int           { return len(count) }
func (count mostFrequent) Less(i, j int) bool { return count[i].Count >= count[j].Count }
func (count mostFrequent) Swap(i, j int)      { count[i], count[j] = count[j], count[i] }
