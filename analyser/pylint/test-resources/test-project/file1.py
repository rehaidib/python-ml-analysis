import string # deprecated module
constant = 42 # constant with wrong naming

def testFunction() -> int: # no doc-string, wrong type
  return constant + 'test' # adding int to string

testFunctionMisspelled()
