package pylint

import (
	"errors"
	"fmt"
)

var (
	// ErrNoPythonFiles indicates that there are no Python files in the current directory.
	ErrNoPythonFiles = errors.New("no Python files")
	// ErrMessageYAMLFormat indicates that there was a malformed Pylint message and shows expected format.
	ErrMessageYAMLFormat = errors.New("expecting Pylint message in format 'path:line:column:type:symbol:symbolid'")
)

// OutputError is a custom error type for capturing Pylint's output as part of the error.
type OutputError struct {
	err    error
	Output string
}

func (err *OutputError) Error() string {
	return fmt.Sprintf("error parsing Pylint output: %v", err.err)
}

func (err *OutputError) Unwrap() error {
	return err.err
}

func NewOutputError(err error, output string) error {
	return &OutputError{err: err, Output: output}
}
