package pylint

type MessageList []Message

func (l MessageList) ByType(msgType MessageType) MessageList {
	res := MessageList{}
	for _, msg := range l {
		if msg.Type == msgType {
			res = append(res, msg)
		}
	}
	return res
}
