package main

import (
	"fmt"

	"github.com/fatih/color"
	"gitlab.com/bvobart/python-ml-analysis/utils"
)

func (a Analyser) NewAnalysisError(err error) error {
	if err == nil {
		return nil
	}
	return fmt.Errorf(color.RedString("--> Project %d - %s - Analysis Error: ", a.Project.ID, utils.FixedLength(a.Project.Name))+"%w", err)
}

func (a Analyser) NewSetupError(err error) error {
	if err == nil {
		return nil
	}
	return fmt.Errorf(color.RedString("--> Project %d - %s - Setup Error: ", a.Project.ID, utils.FixedLength(a.Project.Name))+"%w", err)
}
