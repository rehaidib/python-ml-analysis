package utils

// IfNotEmpty returns the first parameter iff it is not the empty string.
// Returns the second argument as a fallback otherwise.
func IfNotEmpty(s string, fallback string) string {
	if s != "" {
		return s
	}
	return fallback
}
