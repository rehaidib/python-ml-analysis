package utils

import (
	"strings"

	"github.com/fatih/color"
)

type ColouredBool bool

func (b ColouredBool) String() string {
	if b {
		return color.GreenString("%t", b)
	}
	return color.RedString("%t", b)
}

const maxLength = 32

// FixedLength returns the argument string as a string that has a fixed length,
// either truncating it or padding it with whitespace
func FixedLength(name string) string {
	if len(name) > maxLength {
		return name[:maxLength]
	}

	if len(name) < maxLength {
		return name + strings.Repeat(" ", maxLength-len(name))
	}

	return name
}
