package utils_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/utils"
)

func TestFindGlob(t *testing.T) {
	dir := "../pylint/test-resources/test-project"
	glob := "*.py"
	files := utils.FindFilesIn(dir, glob)
	require.Equal(t, utils.Filenames{"_file.py", "file1.py"}, files)
}

func TestFindGlobDoubleStar(t *testing.T) {
	dir := "../pylint/test-resources/test-project"
	glob := "**/*.py"
	files := utils.FindFilesIn(dir, glob)
	require.Equal(t, utils.Filenames{"_folder/somefile.py", "folder/file2.py"}, files)
}

func TestFindBoth(t *testing.T) {
	dir := "../pylint/test-resources/test-project"
	glob1 := "*.py"
	glob2 := "**/*.py"
	files := utils.FindFilesIn(dir, glob1)
	files = append(files, utils.FindFilesIn(dir, glob2)...)
	require.Equal(t, utils.Filenames{"_file.py", "file1.py", "_folder/somefile.py", "folder/file2.py"}, files)
}

func TestFindPythonFiles(t *testing.T) {
	dir := "../pylint/test-resources/test-project"
	files, err := utils.FindPythonFilesIn(dir)
	require.NoError(t, err)
	require.Equal(t, utils.Filenames{"_file.py", "_folder/somefile.py", "file1.py", "folder/file2.py"}, files)
}

func TestFindPythonFilesParentDir(t *testing.T) {
	dir := "../pylint/test-resources" // no Python files directly in that dir, but there are Python files in its subdirectories.
	files, err := utils.FindPythonFilesIn(dir)
	require.NoError(t, err)
	require.Equal(t, utils.Filenames{"test-project/_file.py", "test-project/_folder/somefile.py", "test-project/file1.py", "test-project/folder/file2.py"}, files)
}

func TestFindIPynbFiles(t *testing.T) {
	dir := "../pylint/test-resources/test-project"
	files, err := utils.FindIPynbFilesIn(dir)
	require.NoError(t, err)
	require.Equal(t, utils.Filenames{}, files)

	dir = "../ipynb/test-resources"
	files, err = utils.FindIPynbFilesIn(dir)
	require.NoError(t, err)
	require.Equal(t, utils.Filenames{
		"InterviewNewlineError.ipynb",
		"InterviewNewlineErrorFixed.ipynb",
		"sample.ipynb",
	}, files)

	dir = "../ipynb" // no IPynb files directly in that dir, but there are IPynb files in its subdirectories.
	files, err = utils.FindIPynbFilesIn(dir)
	require.NoError(t, err)
	require.Equal(t, utils.Filenames{
		"test-resources/InterviewNewlineError.ipynb",
		"test-resources/InterviewNewlineErrorFixed.ipynb",
		"test-resources/sample.ipynb",
	}, files)
}

func TestPrefix(t *testing.T) {
	filenames := utils.Filenames{"file1.py", "folder/file2.py"}
	prefixed := filenames.Prefix("something/test-dir")

	expected := utils.Filenames{"something/test-dir/file1.py", "something/test-dir/folder/file2.py"}
	require.Equal(t, len(filenames), len(prefixed))
	require.Equal(t, expected, prefixed)
}
