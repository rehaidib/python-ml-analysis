package utils

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

// CountLinesInReader counts the number of lines in an io.Reader by counting the number of newline characters.
// Inspiration from: https://stackoverflow.com/questions/24562942/golang-how-do-i-determine-the-number-of-lines-in-a-file-efficiently
func CountLinesInReader(r io.Reader) (int, error) {
	const lineBreak = '\n'
	var count int

	buffer := make([]byte, bufio.MaxScanTokenSize)

	for {
		bufferSize, err := r.Read(buffer)
		if err != nil && err != io.EOF {
			return 0, err
		}

		var bufferPosition int
		for {
			i := bytes.IndexByte(buffer[bufferPosition:], lineBreak)
			if i == -1 || bufferSize == bufferPosition {
				break
			}
			bufferPosition += i + 1
			count++
		}
		if err == io.EOF {
			break
		}
	}

	return count, nil
}

// CountLinesInFile counts the number of lines in a file. Same as running `wc -l filename` in shell
func CountLinesInFile(filename string) (int, error) {
	file, err := os.Open(filename)
	if err != nil {
		return 0, err
	}

	return CountLinesInReader(file)
}

// CountLinesInFiles counts the total number of lines in a set of files.
func CountLinesInFiles(filenames []string) (int, error) {
	total := 0
	for _, filename := range filenames {
		count, err := CountLinesInFile(filename)
		if err != nil {
			return 0, fmt.Errorf("failed to count lines in %s: %w", filename, err)
		}
		total += count
	}

	return total, nil
}
