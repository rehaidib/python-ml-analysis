package utils

import (
	"os"
	"path/filepath"
	"strings"
)

// FileExists checks if `filename` describes an existing file on the file system
// Returns false for directories.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists checks if `dir` describes an existing directory on the file system
func DirExists(dir string) bool {
	info, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// OpenOrCreate either opens the file with the given name, or creates it and then opens it.
func OpenOrCreate(filename string) (*os.File, error) {
	file, err := os.Open(filename)
	if os.IsNotExist(err) {
		return os.Create(filename)
	}
	return file, err
}

// FindFilesIn finds all files in the given directory with a filename that matches
// the given glob pattern. Returns their filepaths, relative to the given directory.
func FindFilesIn(dir string, glob string) Filenames {
	files, _ := filepath.Glob(filepath.Join(dir, glob))
	for i, file := range files {
		files[i], _ = filepath.Rel(dir, file)
	}
	return files
}

// FindPythonFilesIn finds all Python (*.py) files in the given directory and subdirectories
// Returns their filepaths, relative to the given directory
// Ignores hidden folders (folders whose names start with a '.'), but not hidden files.
func FindPythonFilesIn(dir string) (Filenames, error) {
	return FindFilesByExtInDir(dir, ".py")
}

// FindIPynbFilesIn finds all Jupyter Notebook (*.ipynb) files in the given directory and
// subdirectories. Returns their filepaths, relative to the given directory.
// Ignores hidden folders (folders whose names start with a '.'), but not hidden files.
func FindIPynbFilesIn(dir string) (Filenames, error) {
	return FindFilesByExtInDir(dir, ".ipynb")
}

// FindFilesByExtInDir finds all files in the given directory and subdirectories that have
// a certain file extension. File extension must start with a '.', e.g. ".py" or ".ipynb"
// Returns filepaths relative to the given directory.
// Ignores hidden folders (folders whose names start with a '.'), but not hidden files.
func FindFilesByExtInDir(dir string, extension string) (Filenames, error) {
	files := Filenames{}
	err := filepath.Walk(dir, func(path string, file os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if file.IsDir() && strings.HasPrefix(file.Name(), ".") {
			return filepath.SkipDir
		}

		if !file.IsDir() && filepath.Ext(path) == extension {
			relpath, _ := filepath.Rel(dir, path)
			files = append(files, relpath)
		}

		return nil
	})

	return files, err
}

// Filenames is simply an alias for []string, but allows me to add some methods.
type Filenames []string

// Prefix prefixes each of the filenames with a directory name.
// i.e. Filenames{"name.py"}.Prefix("something") becomes Filenames{"something/name.py"}
func (names Filenames) Prefix(dir string) Filenames {
	for i, name := range names {
		names[i] = filepath.Join(dir, name)
	}
	return names
}
