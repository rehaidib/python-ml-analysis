package virtualenv_test

import (
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/virtualenv"
)

const testpath = "test-resources/test-venv"

// ensures that the test path is empty
func before(t *testing.T) {
	require.NoError(t, os.RemoveAll(testpath))
	require.NoError(t, os.MkdirAll(testpath, 0755))
}

func TestVirtualEnv(t *testing.T) {
	before(t)
	err := virtualenv.Check(testpath)
	require.Error(t, err)
	require.True(t, errors.Is(err, virtualenv.ErrNoVenvDir))
	require.False(t, virtualenv.Is(testpath))

	venv, err := virtualenv.Create(testpath)
	require.NoError(t, err)
	require.NotNil(t, venv)

	require.NoError(t, virtualenv.Check(testpath))
	require.True(t, virtualenv.Is(testpath))

	venv2, err := virtualenv.Source(testpath)
	require.NoError(t, err)
	require.Equal(t, venv, venv2)
}
