package git_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/bvobart/python-ml-analysis/git"
)

func TestGetContributors(t *testing.T) {
	contributors, err := git.CountContributors(".")
	require.NoError(t, err)
	require.Equal(t, 1, contributors)
}
