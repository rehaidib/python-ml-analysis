module gitlab.com/bvobart/python-ml-analysis

go 1.15

require (
	github.com/fatih/color v1.10.0
	github.com/go-echarts/go-echarts/v2 v2.2.3
	github.com/montanaflynn/stats v0.6.3
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
